<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Gráfico</title>

        <script src="//code.jquery.com/jquery-1.9.1.min.js"></script>
        <script src="http://code.highcharts.com/stock/highstock.js"></script>
        <script src="http://code.highcharts.com/stock/modules/exporting.js"></script>
        <script type="text/javascript">
            $(function () {
                
                Highcharts.setOptions({
                    global: {
                        timezoneOffset: -3,
                        useUTC: false
                        }
                    });
                
                $.getJSON('grafico/dados', function (data) {

                    // create the chart
                    $('#container').highcharts('StockChart', {
                        rangeSelector : {
                            buttons : [{
                                type : 'hour',
                                count : 1,
                                text : '1h'
                            }, {
                                type : 'day',
                                count : 1,
                                text : '1D'
                            }, {
                                type : 'all',
                                count : 1,
                                text : 'All'
                            }],
                            selected : 0,
                            inputEnabled : false
                        },
                        title: {
                            text: 'Indices do Livro de Ofertas'
                        },
                        series: [{
                                type: 'candlestick',
                                name: 'AAPL Stock Price',
                                data: data,
                                dataGrouping: {
                                    units: [
                                        [
                                            'week', // unit name
                                            [1] // allowed multiples
                                        ]
//                                        , [
//                                            'month',
//                                            [1, 2, 3, 4, 6]
//                                        ]
                                    ]
                                }
                            }]
                    });
                });
            });

        </script>
    </head>
    <body>
        <h1>Gráfico</h1>
        <div id="container" style="height: 600px; min-width: 310px"></div>
    </body>
</html>
