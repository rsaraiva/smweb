package com.rsaraiva.labs.smweb.service;

import com.rsaraiva.labs.smweb.util.ConnectionFactory;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import javax.inject.Inject;

public class IndicesService {
    
    @Inject
    private JsonService jsonService;
    
    public String listaJsonDeIndices(String ativo) {
        
        Map<Long, List<Long>> indices = consultaEAgrupaIndicesNoMap(ativo);
        
        return jsonService.converteMapDe(indices);
    }

    public Map<Long, List<Long>> consultaEAgrupaIndicesNoMap(String ativo) {
        
        Map<Long, List<Long>> indices = new TreeMap<>();
        
        try (Connection connection = new ConnectionFactory().getConnection()) {
            String sql = "select instante, indice from indices where instante between '2014-12-04 00:00:00' and '2014-12-04 23:59:59' order by instante";
            PreparedStatement prepareStatement = connection.prepareStatement(sql);
            //prepareStatement.setString(1, ativo);
            ResultSet rs = prepareStatement.executeQuery(sql);
            while (rs.next()) {
                
                Timestamp timestamp = rs.getTimestamp("instante");
                Long indice = rs.getLong("indice");
                
                Long timestampDoMinuto = calculaTimestampDoMinuto(timestamp.getTime());
                
                if (!indices.containsKey(timestampDoMinuto)) 
                    indices.put(timestampDoMinuto, new ArrayList());
                
                indices.get(timestampDoMinuto).add(indice);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return indices;
    }

    private Long calculaTimestampDoMinuto(long milis) {
        Calendar c = Calendar.getInstance();
        c.setTimeInMillis(milis);
        c.set(Calendar.SECOND, 0);
        return c.getTime().getTime();
    }
}
