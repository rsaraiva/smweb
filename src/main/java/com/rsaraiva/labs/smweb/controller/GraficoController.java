package com.rsaraiva.labs.smweb.controller;

import br.com.caelum.vraptor.Controller;
import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.view.Results;
import com.rsaraiva.labs.smweb.service.IndicesService;
import javax.inject.Inject;

@Controller
public class GraficoController {
    
    @Inject Result result;
    @Inject IndicesService indicesService;

    @Path("/")
    public void grafico() {
        
    }
    
    @Get("/grafico/dados")
    public void getDados() {
        String json = indicesService.listaJsonDeIndices("BBSE3");
        result.use(Results.http()).body(json);
    }
}
