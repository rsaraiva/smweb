package com.rsaraiva.labs.smweb.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class IndiceAgrupado {
    
    private final Long instante;
    private final List<Long> indices = new ArrayList<>();

    public IndiceAgrupado(Long instante) {
        this.instante = instante;
    }
    
    public void addIndice(Long indice) {
        indices.add(indice);
    }

    public Long getInstante() {
        return instante;
    }

    public List<Long> getIndices() {
        return Collections.unmodifiableList(indices);
    }
}
