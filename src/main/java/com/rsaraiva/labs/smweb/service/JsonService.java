package com.rsaraiva.labs.smweb.service;

import java.util.List;
import java.util.Map;
import org.json.JSONArray;

public class JsonService {

    public String converteMapDe(Map<Long, List<Long>> indices) {
        
        JSONArray root = new JSONArray();
        
        for (Map.Entry<Long, List<Long>> entrySet : indices.entrySet()) {
            Long minuto = entrySet.getKey();
            List<Long> indicesDoMap = entrySet.getValue();

            JSONArray params = new JSONArray();
            params.put(minuto);
            
            for (Long indice : indicesDoMap) {
                params.put(indice);
            }
            
            root.put(params);
        }
        
        System.out.println(root.toString());
        return root.toString();
    }
    
           
}
